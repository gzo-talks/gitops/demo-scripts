#!/usr/bin/env bash

########################
# include the magic
########################
. ~/bin/demo-magic.sh


########################
# Configure the options
########################

#
# speed at which to simulate typing. bigger num = faster
#
TYPE_SPEED=30

#
# custom prompt
#
# see http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html for escape sequences
#
DEMO_PROMPT="${GREEN}➜ ${CYAN}\W "

# hide the evidence
clear
# cleanup before
# gcloud config set project $PROJECT_ID
# gcloud iam service-accounts delete gitos-agent@$PROJECT_ID.iam.gserviceaccount.com --quiet

export PROJECT_ID=team-1-236319
echo "Your current project is $PROJECT_ID"
echo "Don't forget to do a 'google auth login' first"

pe "open https://github.com/weaveworks/flux"
rm -rf flux  2> /dev/null
pe "git clone https://github.com/weaveworks/flux"
pe "ls -asl flux"
# Create ssh key for git
pe "open https://github.com/weaveworks/flux/blob/master/site/faq.md#how-do-i-use-my-own-deploy-key"
pe "ssh-keygen -q -N '' -f tmp/flux-git-key"
pe "cat tmp/flux-git-key.pub"
pe "open https://gitlab.com/gzo-talks/gitops/k8s/-/settings/repository"
pe "kubectl create secret generic flux-git-deploy --from-file=identity=tmp/flux-git-key --dry-run -o yaml > tmp/flux-git-deploy.yaml"
pe "cat tmp/flux-git-deploy.yaml"
# Create deployments
clear
pe "code files/flux-deployment-dev.yaml"
# DEV
pe "kubectl config use-context gke_team-1-236319_europe-west3-c_gitops-dev"
pe "kubectl create clusterrolebinding cluster-admin-$(whoami) --clusterrole=cluster-admin --user=$(gcloud config get-value core/account)"
pe "cp files/flux-deployment-dev.yaml flux/deploy/flux-deployment.yaml"
pe "kubectl apply -f flux/deploy"
pe "kubectl apply -f tmp/flux-git-deploy.yaml"
pe "kubectl delete $(kubectl get pod -o name -l name=flux)"
# QA
pe "kubectl config use-context gke_team-1-236319_europe-west3-c_gitops-qa"
pe "kubectl create clusterrolebinding cluster-admin-$(whoami) --clusterrole=cluster-admin --user=$(gcloud config get-value core/account)"
pe "cp files/flux-deployment-qa.yaml flux/deploy/flux-deployment.yaml"
pe "kubectl apply -f flux/deploy"
pe "kubectl apply -f tmp/flux-git-deploy.yaml"
pe "kubectl delete $(kubectl get pod -o name -l name=flux)"
# PROD
pe "kubectl config use-context gke_team-1-236319_europe-west3-c_gitops-prod"
pe "kubectl create clusterrolebinding cluster-admin-$(whoami) --clusterrole=cluster-admin --user=$(gcloud config get-value core/account)"
pe "cp files/flux-deployment-prod.yaml flux/deploy/flux-deployment.yaml"
pe "kubectl apply -f flux/deploy"
pe "kubectl apply -f tmp/flux-git-deploy.yaml"
pe "kubectl delete $(kubectl get pod -o name -l name=flux)"
