#!/bin/bash
for context in gke_team-1-236319_europe-west3-c_gitops-dev gke_team-1-236319_europe-west3-c_gitops-qa gke_team-1-236319_europe-west3-c_gitops-prod
do
  kubectl delete deployment,service,ingress,secret --all --context=$context
  kubectl delete clusterrolebinding cluster-admin-$(whoami) --context=$context
  kubectl delete clusterrolebinding flux --context=$context
done

pushd ../k8s
git fetch
git tag -l | xargs -n 1 git push --delete origin
git tag | xargs git tag -d
popd
