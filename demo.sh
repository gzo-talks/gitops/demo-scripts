#!/usr/bin/env bash

########################
# include the magic
########################
. ~/bin/demo-magic.sh


########################
# Configure the options
########################

#
# speed at which to simulate typing. bigger num = faster
#
TYPE_SPEED=30

#
# custom prompt
#
# see http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html for escape sequences
#
DEMO_PROMPT="${GREEN}➜ ${CYAN}\W "
export PROJECT_ID=team-1-236319

# hide the evidence
clear
# cleanup before
# gcloud config set project $PROJECT_ID
# gcloud iam service-accounts delete gitos-agent@$PROJECT_ID.iam.gserviceaccount.com --quiet
clear

echo "Your current project is $PROJECT_ID"
echo "Don't forget to do a 'google auth login' first"

pushd ../micronaut-app
git pull && cp attic/step1/.gitlab-ci.yml . && git commit -am "back to step1" && git push
popd
clear

pe "open https://gitlab.com/gzo-talks/gitops"
pe "open https://gitlab.com/gzo-talks/gitops/demo-app"
pe "pushd ../micronaut-app"
git revert && git pull
cp attic/step1/.gitlab-ci.yml .
pe "cat .gitlab-ci.yml"
pe "open https://gitlab.com/gzo-talks/gitops/demo-app/pipelines"
pe "open https://cloud.docker.com/u/gzockoll/repository/docker/gzockoll/gitops-demo-app"
pe "open https://console.cloud.google.com/kubernetes/list?project=team-1-236319"
pe "kubectl run gitops-demo --image=gzockoll/gitops-demo-app --port 8080"
pe "kubectl expose deployment gitops-demo --port 8080 --name=demo-http --type=LoadBalancer"
pe "kubectl delete all -l run=gitops-demo"

clear
cp attic/step2/.gitlab-ci.yml .
pe "cat .gitlab-ci.yml"
pe "git commit -am 'gitops step added' && git push"
popd
