#!/bin/bash

for cluster in dev qa prod
do
  gcloud container clusters create gitops-$cluster \
    --zone europe-west3-c \
    --preemptible \
    --num-nodes=2 \
    --enable-autoscaling \
    --min-nodes 2 \
    --max-nodes 6 \
    --machine-type=g1-small &
done
