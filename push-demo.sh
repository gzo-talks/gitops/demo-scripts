#!/usr/bin/env bash

########################
# include the magic
########################
. ~/bin/demo-magic.sh


########################
# Configure the options
########################

#
# speed at which to simulate typing. bigger num = faster
#
TYPE_SPEED=30

#
# custom prompt
#
# see http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html for escape sequences
#
DEMO_PROMPT="${GREEN}➜ ${CYAN}\W "
export PROJECT_ID=team-1-236319

# hide the evidence
clear
# cleanup before
# gcloud config set project $PROJECT_ID
# gcloud iam service-accounts delete gitos-agent@$PROJECT_ID.iam.gserviceaccount.com --quiet
clear

echo "Your current project is $PROJECT_ID"
echo "Don't forget to do a 'google auth login' first"

clear
pe "gcloud config set project $PROJECT_ID"
pe "open https://console.cloud.google.com/kubernetes/list?project=team-1-236319"
p "gcloud iam service-accounts create gitops-agent --display-name 'Gitops Agent'"
p "gcloud projects add-iam-policy-binding $PROJECT_ID --member serviceAccount:gitops-agent@$PROJECT_ID.iam.gserviceaccount.com --role roles/editor"
p "gcloud projects add-iam-policy-binding $PROJECT_ID --member serviceAccount:gitops-agent@$PROJECT_ID.iam.gserviceaccount.com --role roles/container.admin"
p "gcloud projects add-iam-policy-binding $PROJECT_ID --member serviceAccount:gitops-agent@$PROJECT_ID.iam.gserviceaccount.com --role roles/storage.admin"
p "gcloud iam service-accounts keys create creds/serviceaccount.json --iam-account gitops-agent@$PROJECT_ID.iam.gserviceaccount.com"
pe "cat creds/serviceaccount.json | base64"
pe "open https://gitlab.com/groups/gzo-talks/gitops/-/settings/ci_cd"

pe "open https://gitlab.com/gzo-talks/gitops/k8s/blob/develop/backup/.gitlab-ci.yml"
pe "pushd ../k8s"
pe "cp backup/.gitlab-ci.yml . && git commit -am 'enable ci' && git push"
pe "open https://gitlab.com/gzo-talks/gitops/k8s/pipelines"
popd
